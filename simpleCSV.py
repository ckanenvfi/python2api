#!/usr/bin/env python
import requests
import json
import sys
import os
from xml.etree import ElementTree
import urllib
try:
    from urllib.parse import urlparse
except ImportError:
     from urlparse import urlparse

# params = {
#     # 'organization': "8741f861-6384-445e-baef-5fe38bc43caa"
#     'organization': "syke-reporting"
# }
proxies = {
    'http': 'http://gate102.vyh.fi:81',
    'https': 'http://gate102.vyh.fi:81'
}
s=requests.Session()
s.proxies = proxies
s.headers = {'Content-Type':'text/html; charset=utf-8'}
# url = 'https://ckan.ymparisto.fi/api/3/action/package_search'
# response = s.post(url, params
response = s.get('https://ckan.ymparisto.fi/api/3/action/package_search?rows=1000', verify=False)
response.raise_for_status()
# response = requests.post(url, params)
if(response.status_code!=200):
    print('request failed for %s, status code %s' % (url, str(response.status_code)))
    sys.exit()
else:
    print('request succeeded. maybe.')
response.encoding = 'utf-8'
response_dict = json.loads(response.content)
if (not response_dict['success']):
    print(response_dict['error']['message'])
    sys.exit()

print(str(response_dict['result']['count']) + ' packages found')

results = response_dict['result']['results']
output = 'Otsikko;Kuvaus;Metatiedosta vastaavan organisaation yhteystieto;Aineistosta tai järjestelmästä vastaavan organisaation yhteystieto;Resurssityyppi;Metatieto päivitetty\n'
#Paikkatiedot ja kaukokartoitus : 4244b515-9d5d-4637-a225-d2ee5e484beb
#Ympäristötietojärjestelmät ja -varannot : 1482f361-cc5d-4695-9e84-500f863411ba
#Karttapalvelut : f2d61993-c9c1-4629-9994-f23b288f0be0
#Muut : e346817e-bf54-4a15-b63f-746a6d482df6
haettavatOrganisaatiot = ["4244b515-9d5d-4637-a225-d2ee5e484beb", "1482f361-cc5d-4695-9e84-500f863411ba", "f2d61993-c9c1-4629-9994-f23b288f0be0", "e346817e-bf54-4a15-b63f-746a6d482df6"]
counter = 0
for result in results:
    if result['owner_org'] in haettavatOrganisaatiot:
        tietotyyppi = '-'
        if result['organization']:
            tietotyyppi = result['organization'].get('title','-')
        kuvaus = result.get('notes','-')[0:300].replace('\n',' * ').replace(';',',')
        metaEmail='-'
        ownerEmail='-'
        metadatadate='-'
        for extra in result['extras']:
            if(extra['key']=='metadata-responsible-party'):
                respParty = json.loads(extra['value'])
                metaEmail=respParty[0]['email']
            if(extra['key']=='responsible-party'):
                respParty = json.loads(extra['value'])
                ownerEmail=respParty[0]['email']
            if(extra['key']=='metadata-date'):
                metadatadate = extra['value']
        output = output + result.get('title','-') + ';' + kuvaus + ';' + metaEmail + ';' + ownerEmail + ';' + tietotyyppi + ';' + metadatadate + '\n'
        counter = counter +1

with open('ckandata.txt', 'w', encoding='utf-8') as file:
    file.write(output)

print(str(counter) + ' packages written in csv')