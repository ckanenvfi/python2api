#!/usr/bin/env python
import requests
import json
import sys
import os
from xml.etree import ElementTree
import urllib
try:
    from urllib.parse import urlparse
except ImportError:
     from urlparse import urlparse

params = {
    # 'organization': "8741f861-6384-445e-baef-5fe38bc43caa"
    'organization': "syke-reporting"
}
proxies = {
    'http': 'http://gate102.vyh.fi:81',
    'https': 'http://gate102.vyh.fi:81'
}
s=requests.Session()
s.proxies = proxies
# url = 'https://ckan.ymparisto.fi/api/3/action/package_search'
# response = s.post(url, params
response = s.get('https://ckan.ymparisto.fi/api/3/action/package_search?rows=200', verify=False)
response.raise_for_status()
# response = requests.post(url, params)
if(response.status_code!=200):
    print('request failed for %s, status code %s' % (url, str(response.status_code)))
    sys.exit()
else:
    print('request succeeded. maybe.')
response_dict = json.loads(response.content)
if (not response_dict['success']):
    print(response_dict['error']['message'])
    sys.exit()

print(str(response_dict['result']['count']) + ' packages found')

results = response_dict['result']['results']
output = 'Raportin nimi;Vastuuorganisaatio;Vastuutaho SYKEssa'
for result in results:
    if result['owner_org'] == "8741f861-6384-445e-baef-5fe38bc43caa":
        output = output + result.get('title','-') + ';' + result.get('reporting_organization','-') + ';' + result.get('reporting_organization_unit','-') + '\n'

with open('envireports.txt', 'w') as file:
    file.write(output)