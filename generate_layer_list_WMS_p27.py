#!/usr/bin/env python
import requests
import json
import sys
import io
import os
from shutil import copyfile
from xml.etree import ElementTree
import urllib
try:
    from urllib.parse import urlparse
    from urllib.parse import unquote
except ImportError:
     from urlparse import urlparse
     from urlparse import unquote




def get_WMS_layer_list(url):
    '''
    Retrieves the capabilities doc of an WFS service and parses an
    array of the layers/feature types available.
    '''
    try:
        layerArray = []
        proxies = {
            'http': 'http://gate102.vyh.fi:81',
            'https': 'http://gate102.vyh.fi:81'
        }
        s=requests.Session()
        s.proxies = proxies
        url = (url + '?') if "?" not in url else url
        url = (url + '&request=GetCapabilities') if "request=getcapabilities" not in url.lower() else url
        url = (url + '&service=WMS') if "service=wms" not in url.lower() else url
        url = (url + '&version=1.3.0') if "version=" not in url.lower() else url
        baseurl = url.split('?')[0]
        r = s.get(url, verify=False)
        if(r.status_code!=200):
            print('get_WMS_layer_list failed for %s, status code %s' % (url, str(r.status_code)))
            return {}
        #tree = ElementTree.fromstring(r.content.decode("utf-8"))
        #tree = ElementTree.fromstring(str(unicode(r.content).encode('utf-8')))
        tree = ElementTree.fromstring(r.content)
        #print(r.content)
        roottag = tree.tag
        rootnamespace = ''
        if roottag[0] == '{':
            rootnamespace = '{' + roottag[1:].split('}')[0] + '}'
        capa = tree.find(rootnamespace + 'Capability')
        index = 0
        for f in capa.iter(rootnamespace + 'Layer'):
            try:
                index=index+1
                if (f.find(rootnamespace + 'Name')) is not None:
                    name = f.find(rootnamespace + 'Name').text
                    if (f.find(rootnamespace + 'Title')) is not None:
                        title = f.find(rootnamespace + 'Title').text
                    else:
                        title = name
                    layer = {
                        'name':name,
                        'title':title,
                    }
                    try:
                        for leg in f.iter(rootnamespace + 'LegendURL'):
                            legendurl = leg.find(rootnamespace + 'OnlineResource').attrib['{http://www.w3.org/1999/xlink}href']
                            layer['legendURL'] = legendurl
                    except:
                        pass
                    
                    layerArray.append(layer)
            except Exception as err:
                print('[get_WMS_layer_list] Error parsing WMS capabilities doc (index ' + str(index) + ')')

        return layerArray
    except Exception as e:
        print('[get_WMS_layer_list] WMS layer listing for %s failed with exception: %s' % (url, str(e)))
    return {}


##ll = get_WMS_layer_list('http://paikkatieto.ymparisto.fi/arcgis/services/INSPIRE/SYKE_SuojellutAlueet/MapServer/WMSServer?request=GetCapabilities&service=WMS')
##print(ll)
##sys.exit()



def guess_resource_format(url):
    '''
    Given a URL try to guess the best format to assign to the resource

    The function looks for common patterns in popular geospatial services and
    file extensions, so it may not be 100% accurate. It just looks at the
    provided URL, it does not attempt to perform any remote check.

    Returns None if no format could be guessed.

    '''
    url = url.lower().strip()

    resource_types = {
        # OGC
        'wms': ('service=wms', 'geoserver/wms', 'mapserver/wmsserver', 'com.esri.wms.Esrimap', 'service/wms'),
        'wfs': ('service=wfs', 'geoserver/wfs', 'mapserver/wfsserver', 'com.esri.wfs.Esrimap'),
        'wcs': ('service=wcs', 'geoserver/wcs', 'imageserver/wcsserver', 'mapserver/wcsserver'),
        'sos': ('service=sos',),
        'csw': ('service=csw',),
        # ESRI
        'kml': ('mapserver/generatekml',),
        'arcims': ('com.esri.esrimap.esrimap',),
        'arcgis_rest': ('arcgis/rest/services',),
    }

    for resource_type, parts in resource_types.items():
        if any(part in url for part in parts):
            return resource_type

    if ('wfs?' in url.lower()) and 'request=getfeature' in url.lower():
        return 'wfs'
    
    if ('wms?' in url.lower()) and 'request=getmap' in url.lower():
        return 'wms'
    
    file_types = {
        'kml' : ('kml',),
        'kmz': ('kmz',),
        'gml': ('gml',),
    }

    for file_type, extensions in file_types.items():
        if any(url.endswith(extension) for extension in extensions):
            return file_type

    return None



# *** MAIN ***

# ckanhost = 'http://ckandevelmtp.env.fi'  
ckanhost = 'https://ckanmtp.ymparisto.fi'
#ckanhost = 'https://10.108.5.56'
#ckanhost = 'https://ckan.ymparisto.fi'

if len(sys.argv) > 1:
    filename = sys.argv[1]
else:
    filename = 'layers_test.json'
    
outputDirectory = '\\\\tiw01\\wwwroot\\MeriPortaaliPowerBI\\'

# backups
nrOfBackups=7
backUpMaxSize=524289 #0.5 MB
if os.stat(outputDirectory + filename).st_size<backUpMaxSize:
    filename_parts = filename.split('.')
    for x in range(nrOfBackups,1,-1):
        try:
            src = outputDirectory + filename_parts[0] + '_' + str(x-1)
            dst = outputDirectory + filename_parts[0] + '_' + str(x)
            if len(filename_parts)>1:
                src = src + '.' + filename_parts[1]
                dst = dst + '.' + filename_parts[1]
            copyfile(src, dst)
        except:
            pass
    try:
        src = outputDirectory + filename
        dst = outputDirectory + filename_parts[0] + '_1'
        if len(filename_parts)>1:
            dst = dst + '.' + filename_parts[1]
        copyfile(src, dst)
    except:
        pass

# request    
try:
    #TODO: retrieve in batches (limit & offset)
    proxies = {
        'http': 'http://gate102.vyh.fi:81',
        'https': 'http://gate102.vyh.fi:81'
    }
    s=requests.Session()
    s.proxies = proxies
    response = s.get(ckanhost +'/api/3/action/package_search?rows=200', verify=False)
    response.raise_for_status()
except Exception as err:
    print('The request failed: ' + str(err))
    sys.exit()

#print(response.content)

response_dict = json.loads(response.content)

if (not response_dict['success']):
    print(response_dict['error']['message'])
    sys.exit()

print(str(response_dict['result']['count']) + ' packages found')

results = response_dict['result']['results']
output = {'layers':[]}
for result in results:

    print('')
    print('Package ' + result['name'] + ':')
    if not result['num_resources'] > 0:
        print('   no resources')
        continue

    group = 'Muut'
    if(result['groups'] and len(result['groups'])>0):
        group = result['groups'][0]['display_name']
        
    organizationTitle = None
    organizationName = None
    if(result['organization']):
        organizationTitle = result['organization']['title']
        organizationName = result['organization']['name']

    #wmsFound=False;
    wmsResourceCount = 0
    alreadyInserted = []
    for resource in result['resources']:
        #find the resource/resources to use as layers
        layerType = guess_resource_format(resource['url'])
        if layerType!='wms':
            continue
        base_url = unquote(resource['url'])
        base_url = base_url.replace(' ', '')
        layerArray = []
        #find the actual layer/s
        # first: CKAN-style
        if '#' in resource['url']:
            base_url = base_url.split('#')[0]
            layerArray = base_url.split('#')[1].split(',')
        # second: standard WMS
        if '?' in base_url:
            strParams = base_url.split('?')[1]
            base_url = base_url.split('?')[0]
            paramPairs = strParams.split('&')
            for pair in paramPairs:
                #params[pair.split('=')[0]]=pair.split('=')[1]
                if '=' in pair and pair.split('=')[0].lower()=='layers':
                    layerArray = pair.split('=')[1].split(',')
        print('   ' + base_url + ':')
        print('     - ' + str(len(layerArray)) + ' layers found in resource URL')

        # get capabilities
        layerProperties = get_WMS_layer_list(base_url)
        print('     - ' + str(len(layerProperties)) + ' layers found in capabilities')

        # prepare layer definitions for karttasovellus    
        i = 0
        if len(layerArray)>0:
            for index, lyr in enumerate(layerArray):
                layerProp = None
                for lp in layerProperties:
                    shortName = lp['name']
                    # if ':' in shortName:
                    #     shortName = shortName.split(':')[1]
                    if lp['name']==lyr or shortName==lyr:
                        layerProp = lp
                        break
                    
    ##            if layerProperties[lyr]:
    ##                layer = {
    ##                    'id': resource['id'] + '_' + str(index),
    ##                    'name': layerProperties[lyr]['title'],
    ##                    'type': 'wmslayer',
    ##                    'url': base_url,
    ##                    'layerName': lyr,
    ##                    'ckanmetaurl': ckanhost + '/dataset/' + result['id'] + '/resource/' + resource['id']
    ##                }
    ##                if layerProperties[lyr]['legendURL']:
    ##                    layer['legendImage'] = layerProperties[lyr]['legendURL']
                if layerProp and (base_url + '/' + layerProp['name']) not in alreadyInserted:
                    alreadyInserted.append(base_url + '/' + layerProp['name'])
                    layer = {
                        'id': resource['id'] + '_' + str(index),
                        'resourceId': resource['id'],
                        # 'title': layerProp['title'],
                        'layerName': layerProp['name'],
                        'url': base_url,
                        'type': 'wmslayer',
                        # 'metadata': ckanhost + '/dataset/' + result['id'] + '/resource/' + resource['id'],
                        'metadata': ckanhost + '/dataset/' + result['id'],
                        'group': group,
                        'organizationTitle': organizationTitle,
                        'organizationName': organizationName
                    }
                    if layerProp['title']:
                        layer['title'] = layerProp['title']
                    else:
                        layer['title'] = layerProp['name']
                    if layerProp['legendURL']:
                        layer['legendImage'] = layerProp['legendURL']
                    output['layers'].append(layer)
                    # print('         * ' + layerProp['name'])
                    i+=1
                    wmsResourceCount+=1
        # don't add all layers in the interface!!! could be 122
        # else:
            # for layerProp in layerProperties:
            #     if (base_url + '/' + layerProp['name']) not in alreadyInserted:
            #         alreadyInserted.append(base_url + '/' + layerProp['name'])
            #         layer = {
            #             'id': resource['id'] + '_' + str(i),
            #             'resourceId': resource['id'],
            #             'layerName': layerProp['name'],
            #             'url': base_url,
            #             'type': 'wmslayer',
            #             # 'metadata': ckanhost + '/dataset/' + result['id'] + '/resource/' + resource['id'],
            #             'metadata': ckanhost + '/dataset/' + result['id'],
            #             'group': group,
            #             'organizationTitle': organizationTitle,
            #             'organizationName': organizationName
            #         }
            #         if layerProp['title']:
            #             layer['title'] = layerProp['title']
            #         else:
            #             layer['title'] = layerProp['name']
            #         if layerProp['legendURL']:
            #             layer['legendImage'] = layerProp['legendURL']
            #         output['layers'].append(layer)
            #         print('         * ' + layerProp['name'])
            #         i+=1
            #         wmsResourceCount+=1
        if wmsResourceCount > 0:
            print('     - ' + str(wmsResourceCount) + ' layers added to list')
        else:
            print('   no wms-resources')

with io.open(filename, 'w', encoding='utf8') as f:
    #json.dump(output, f, ensure_ascii=False)
    f.write(unicode(json.dumps(output, ensure_ascii=False)))


