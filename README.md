# Python2API

Tässä repositorissa on erilaisia pieniä skriptejä ja testejä, jotka hyödyntävät CKANin web-APIa. Skripti generate_layer_list_WMS.py hakee annetusta CKAN-API-osoitteesta kaikki WMS-resurssit listaksi, jota käytetään Meritietoportaalin katselukarttapalvelussa näkyvien tasojen määrittelyyn. Skripti multilingual_update.py on esimerkki siitä, miten CKANiin voidaan lisätä kieliversiointia myös ryhmille ja tägeille (käytännössä toimivat CKANissa aika rajallisesti). Skripti test_query.py on yksinkertaisen API-kyselyn testi.

Koodi on kirjoitettu Python-versiolle 3.7 ja toimii ainakin Anacondan kanssa, ajettuna Anaconda Prompt-ikkunassa.


## Usage

python generate_layer_list_WMS.py [output_filename]

Jos tulostiedoston nimeä ei anna, lista tallennetaan samaan hakemistoon nimellä "layers.json". CKAN-palvelimen osoite on tässä versiossa kovakoodattu.
