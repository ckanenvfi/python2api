#!/usr/bin/env python
import requests
import json
import sys
from xml.etree import ElementTree
import urllib
try:
    from urllib.parse import urlparse
except ImportError:
     from urlparse import urlparse

params = {
    'q':'text:*meren*',
    'rows':'2'
}
# proxies = {
#     'http': 'http://gate102.vyh.fi:81',
#     'https': 'http://gate102.vyh.fi:81'
# }
s=requests.Session()
# s.proxies = proxies
url = 'http://ckandevelmtp.env.fi/api/3/action/package_search'
response = s.post(url, params)
# response = requests.post(url, params)
if(response.status_code!=200):
    print('request failed for %s, status code %s' % (url, str(response.status_code)))
    sys.exit()
else:
    print('request succeeded. maybe.')
response_dict = json.loads(response.content)
if (not response_dict['success']):
    print(response_dict['error']['message'])
    sys.exit()

print(str(response_dict['result']['count']) + ' packages found')