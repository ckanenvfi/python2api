Raportin nimi;Vastuuorganisaatio;Vastuutaho SYKEssaTulvadirektiivi, 2007/60/EY, 15 artikla, 2019 (2. suunnittelukausi 2016-2021);SYKE;VK
Päästökattodirektiivi (EU) 2016/2284, raportointijakso 1980-2018, raportoitu 2020;SYKE;KTK
Ilman epäpuhtauspäästöt - YK:n kaukokulkeutumissopimukselle toimitettavat tiedot, vuoden 2020 tiedot;SYKE;KTK
Luontodirektiivi, (92/43/ETY), artikla 17, vuosi 2019, raportointijakso 2013-2018;SYKE;BK
Ilman epäpuhtauspäästöt  - YK:n kaukokulkeutumissopimukselle toimitettavat tiedot, vuoden 2019 tiedot;SYKE;KTK
Kaatopaikkadirektiivi (2000/738/EY), raportointijakso 2016-2017, raportoitu 2019;SYKE;KTK
Jätedirektiivi (1004/2019/EY), kierrätysasteiden saavuttaminen vuosina 2016 ja 2017, raportoitu 2019;SYKE;KTK
Päästökattodirektiivi (EU) 2016/2284, raportointijakso 1980-2017, raportoitu 2019;SYKE;KTK
Lietedirektiivi (86/278/ETY), raportointi vuosilta 2016-2018, raportoitu 2019;SYKE;KTK
Jätteensiirtoasetus (EY) N:o 1013/2006;SYKE;KTK
Meridirektiivi (MSD, 2008/56/EY), raportoitu 2018, raportointijakso 2012-2018;-;-
INSPIRE direktiivin (2007/2/EC) vuosiraportit;MML;
Nationally Designated Areas (CDDA);SYKE;TK
Nitraattidirektiivi (NiD 91/676/ETY), raportoitu 2016, raportointijakso 2012-2015;SYKE;VK
Teollisuuspäästödirektiivi (2010/75/EU), raportointijakso 2013-2016, raportoitu 2018;KEHA;KTK
Lietedirektiivi (86/278/ETY), raportointijakso 2013-2015, raportoitu 2016 ja 2017;SYKE;KTK
Kaatopaikkadirektiivi (2000/738/EY), raportointijakso 2013-2015, raportoitu 2016 ja 2017;SYKE;KTK
Jätedirektiivi (2008/98/EY), raportointijakso 2013-2015, raportoitu 2016 ja 2017;SYKE;KTK
Kaivannaisjätedirektiivi (2006/21/EY), raportointijakso 2014-2017, raportoitu 2018;SYKE;KTK
Yhdyskuntajätevesidirektiivi (91/271/ETY), artikla 15, 2018;SYKE;KTK
Yhdyskuntajätevesidirektiivi (91/271/ETY), artikla 17, 2018;SYKE;KTK
